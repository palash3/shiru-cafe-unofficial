package com.example.palash.my_apps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("user_details", 0); // 0 - for private mode
        setContentView(R.layout.user_details);
        Button save_bt =  findViewById(R.id.save);

        final EditText username_str = findViewById(R.id.username);
        final EditText password_str = findViewById(R.id.password);

        username_str.setText(pref.getString("username",null));
        password_str.setText(pref.getString("password",null));

        save_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shiru_cafe_username = String.valueOf(username_str.getText());
                String shiru_cafe_password = String.valueOf(password_str.getText());
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("username",shiru_cafe_username);
                editor.putString("password",shiru_cafe_password);
                editor.commit();
                Toast.makeText(getApplicationContext(),"Saved",Toast.LENGTH_LONG).show();
                Intent main_activity_intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main_activity_intent);
            }
        });
    }
}
