package com.example.palash.my_apps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.setVmPolicy(builder.build());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_layout);
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("user_details", 0); // 0 - for private mode
        final WebView myWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        myWebView.loadUrl("https://mypage.shirucafe.com/user/login?lang=en");
        myWebView.setWebViewClient(new WebViewClient(){
            String current_url;
            int tries=0;
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                current_url=url;
                view.loadUrl(url);
                Log.i("MainActivity",url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i("THe url is ",url);

                if(url.indexOf("login") >0 && tries == 0){
                    final String username = pref.getString("username",null);
                    final String password = pref.getString("password",null);
                    final String js = "javascript:" +
                            "document.getElementById('form_email').value = '" + username + "';"  +
                            "document.getElementById('form_password').value = '" +password  + "';" +
                            "document.forms[0].submit()";

                    if (Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(js, new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {

                            }
                        });
                    } else {
                        view.loadUrl(js);
                    }
                    tries++;
                }
                if(url.indexOf("user/mypage") >0){
                    float cups = pref.getFloat("cups",1);
                    myWebView.loadUrl("https://mypage.shirucafe.com/user/qr?cup="+cups+"&drink=3&in_out=takeout-free&lang=en");
                    cups = cups +1;
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putFloat("cups",cups);
                    editor.commit();
                }

                if(url.indexOf("qr")>0){
                    final String js = "javascript:" +
                            "setTimeout(function(){ document.getElementById('continue').click() },6000)";

                    if (Build.VERSION.SDK_INT >= 19) {
                        view.evaluateJavascript(js, new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {
                                Log.i("Somewhere in callback",s);
                            }
                        });
                    } else {
                        view.loadUrl(js);
                    }
                    Timer myTimer = new Timer();
                    myTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            takeScreenshot();
                        }

                    }, 10000);
                    //takeScreenshot();
                }
                if (url.equals( "https://mypage.shirucafe.com/user/order?lang=en")){
                    float cups = pref.getFloat("cups",0);
                    cups= cups+1.0f;
                    if(Float.compare(cups,3.0f)>0){
                        cups=0.0f;
                    }
                    myWebView.loadUrl("https://mypage.shirucafe.com/user/qr?cup="+cups+"&drink=3&in_out=takeout-free&lang=en");
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putFloat("cups",cups);
                    editor.commit();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_values, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.user_details:
                Intent user_details_activity = new Intent(getApplicationContext(), UserDetailsActivity.class);
                startActivity(user_details_activity);
                return true;
            case R.id.image_view:
                takeScreenshot();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        Log.i("Scrrenshot","IN screenshot function");
        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + "latest.jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            Intent screenshot_viewer_activity = new Intent(getApplicationContext(), Screenshotviewer.class);
            startActivity(screenshot_viewer_activity);
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }
}